//import { posicionVerde } from './fichas.js';

// Objeto con las propiedades...
let obj = {
    mouthCat : document.getElementById("boca-1"), 
    mouthDog:  document.getElementById("boca-2"),
    intro:  document.getElementById("intro"),
    audioIntro: document.getElementById("audio-intro"),

    //Instrucciones
    instrucciones: document.getElementById('instrucciones'),
    registro: document.getElementById('registro'),
    lanzarDado: document.getElementById('lanzar-dado'),

    //Registro
    inputs: document.querySelectorAll('.inputs input'),

    //Lanzar Dado
    caraSeleccionada: document.querySelector("#cara-seleccionada"),
    btnLanzar: document.querySelector('#lanzar'),
    equipoColor: document.querySelector("#color-equipo"),
    equipoColor1: document.querySelector("#color-equipo1"),    
    etiquetaEquipo: document.querySelector('#etiqueta-equipo'),
    nombreEquipo: document.querySelector('#nombre-equipo'),
    etiquetasCategoria: document.querySelectorAll("#categoria label"),

    //mostrar pregunta
    mostrarPregunta: document.querySelector("#mostrar-pregunta"),
    preguntaHTML: document.querySelector('.cuadro-pregunta #pregunta'),
    opcionA: document.querySelector('.respuestas #opcion-a span'),
    opcionB: document.querySelector('.respuestas #opcion-b span'),
    opcionC: document.querySelector('.respuestas #opcion-c span'),

    //mostrar respuestas - ocultar botonera
    botonera: document.querySelector('.botonera'),
    botonera1: document.querySelector('.botonera1'),
    mostrarRespuestas: document.querySelector('.respuestas'),
    contadorSeg: document.querySelector('#tiempo'),
    puntos: 0,
    intervaloTiempo: null,
    seg20: 20,
    seg15: 15,
    preguntaAleatoria: 0,
    categoriaAleatoria: 0,


    // Mostrar tabla de posiciones
    tablaPosiciones: document.querySelector('#tabla-posiciones'),
    tablero:document.querySelector('.tablero'),
    btnSiguienteTurno: document.querySelector('.tablero-btn img'),

    //MostrarFichas
    fichaVerde: document.querySelector('.tablero #ficha-1'),
    fichaAzul: document.querySelector('.tablero #ficha-2'),
    fichaAmarillo: document.querySelector('.tablero #ficha-3'),
    fichaRojo: document.querySelector('.tablero #ficha-4'),
    fichaNegro: document.querySelector('.tablero #ficha-5'),

    //Seccion premiacion    
    tableroPremiacion: document.querySelector('#premiacion'),
    primerImg: document.querySelector('.primer-lugar #posicion1'),
    primerLabel: document.querySelector('.primer-lugar label'),
    segundoImg: document.querySelector('.segundo-lugar #posicion2'),
    segundoLabel: document.querySelector('.segundo-lugar label'),
    tercerImg: document.querySelector('.tercer-lugar #posicion3'),
    tercerLabel: document.querySelector('.tercer-lugar label'),
    cuartoImg: document.querySelector('.cuarto-lugar #posicion4'),
    cuartoLabel: document.querySelector('.cuarto-lugar label'),

    // Variables de juego...
    equipoActual: 0,
    preguntaActual: [],

    orden: [],
    recorrerOrden: 0,
    nombresEquipo: [],
    preguntasCat1: [],
    preguntasCat2: [],
    preguntasCat3: [],
    preguntasCat4: [],
    preguntasCat5: [],
    preguntasCat6: [],
    datosJSON: [],
    puntosError: 0,
    puntosEquipo1: 0,
    puntosEquipo2: 0,
    puntosEquipo3: 0,
    puntosEquipo4: 0,
    categoriasDisponibles: [1,2,3,4,5,6],
    premiacion:[]
   
}


// Objeto con los metodos del juego.
let m = {
    introMascotas: async function(){
        //obj.mouthCat.src = '../recursos/gata/1.png';
        //obj.mouthBear.src = '../recursos/oso/1.png';
        m.playAudio();

        setTimeout(() => {            
            m.animarBocaPerro()
        }, 3000);
        
        setTimeout(() => {
            m.animarBocaGata() 
        }, 10000);
        
        m.ordenEquipos();

        obj.datosJSON = await m.cargarDatosJSON('GET', 'js/file.json');

    },


    cargarDatosJSON: function(method, url){
        return new Promise(function(resolve, reject){
            let xhr = new XMLHttpRequest();
            xhr.open(method, url);
            xhr.onload = function(){
                if (this.status >= 200 && this.status < 300) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            }
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            xhr.send();
        })
    },

    

    animarBocaGata: function(){
        let i = 1
        let intervaloGata = setInterval(() => {
            obj.mouthCat.src = `./recursos/gata/${i}.png`
            i++
            if( i === 4) i = 1;
        }, 230);

        setTimeout(() => {
            clearInterval(intervaloGata);
            obj.mouthCat.src = './recursos/gata/1.png';
        }, 9500);
    },


    animarBocaPerro: function(){
        let j = 1
        let intervaloPerro = setInterval(() => {
            obj.mouthDog.src = `./recursos/perro/${j}.png`;
            j++
            if( j === 4) j = 1;

        }, 230);
        //console.log(intervaloOso)
        setTimeout(() => {
           clearInterval(intervaloPerro);
           obj.mouthDog.src = './recursos/perro/1.png';
        }, 7000);        
    },
   
    
    jugar: function(){
        intro.style.display = 'none';
        instrucciones.style.display = 'block';
    },


    ordenEquipos: function(){
        const equipos = [1, 4, 3, 2];
        obj.orden = equipos.sort(function() { return Math.random() - 0.5 });
    },


    playAudio: function() {
        // const audio = new Audio('./recursos/audio-portada.wav');
        // audio.play();
        console.log(obj.audioIntro);
        obj.audioIntro.play()
    },


    siguienteInstrucciones: () => {
        // Necesito detener toda la animacion....
        intro.style.display = 'none';
        instrucciones.style.display = 'none';
        registro.style.display = 'block';
        //console.log('Desde btn siguiente instrucciones');
    },


    siguienteRegistro: () => {
      
        const [verde, azul, amarillo, rojo] = obj.inputs;

        if (verde.value === '' || azul.value === '' || amarillo.value === '' || rojo.value ==='' ){
            alert('Registra todos los equipos')
        } else {

            obj.nombresEquipo.push(verde.value, azul.value, amarillo.value, rojo.value )
            obj.intro.style.display = 'none';
            obj.instrucciones.style.display = 'none';
            obj.registro.style.display = 'none';
            obj.lanzarDado.style.display = 'block';

            // mostrar nombres de equipo...
            m.mostrarEquipo();

            // llenar categorias
            m.llenarCategorias();

            // llenar preguntas
            m.llenarPreguntas()
        }
    },


    /**
     *  SECCION LANZAR DADO
     * 
     */
   
    llenarCategorias: function(){   
        for (let i = 0; i < obj.datosJSON.mars.length; i++) {
            obj.etiquetasCategoria[i].innerHTML = obj.datosJSON.mars[i].descripcion;
        }
    },


    llenarPreguntas: function(){
        obj.preguntasCat1 = obj.datosJSON.mars[0].questions;
        obj.preguntasCat2 = obj.datosJSON.mars[1].questions;
        obj.preguntasCat3 = obj.datosJSON.mars[2].questions;
        obj.preguntasCat4 = obj.datosJSON.mars[3].questions;
        obj.preguntasCat5 = obj.datosJSON.mars[4].questions;
        obj.preguntasCat6 = obj.datosJSON.mars[5].questions;
    },


    mostrarEquipo: function(){          
        //Reiniciar orden de Equipos..
        if (obj.recorrerOrden === 4) obj.recorrerOrden = 0;

        obj.equipoActual = obj.orden[obj.recorrerOrden]

        switch ( obj.orden[obj.recorrerOrden]) {
            case 1:
                obj.equipoColor.src = './recursos/equipo-color/green.png'
                obj.equipoColor1.src = './recursos/equipo-color/green.png'
                obj.nombreEquipo.innerHTML = obj.nombresEquipo[0];
                obj.etiquetaEquipo.innerHTML = obj.nombresEquipo[0];
                break;

            case 2:
                obj.equipoColor.src = './recursos/equipo-color/blue.png'
                obj.equipoColor1.src = './recursos/equipo-color/blue.png'
                obj.nombreEquipo.innerHTML = obj.nombresEquipo[1];
                obj.etiquetaEquipo.innerHTML = obj.nombresEquipo[1];
                break;

            case 3:
                obj.equipoColor.src = './recursos/equipo-color/yellow.png'
                obj.equipoColor1.src = './recursos/equipo-color/yellow.png'
                obj.nombreEquipo.innerHTML = obj.nombresEquipo[2];
                obj.etiquetaEquipo.innerHTML = obj.nombresEquipo[2];
                break;  

            case 4:
                obj.equipoColor.src = './recursos/equipo-color/red.png'
                obj.equipoColor1.src = './recursos/equipo-color/red.png'
                obj.nombreEquipo.innerHTML = obj.nombresEquipo[3];
                obj.etiquetaEquipo.innerHTML = obj.nombresEquipo[3];
                break;
        
            default:
                break;
        }  
    },


    lanzarDado: function(){
        obj.caraSeleccionada.style.opacity = 0
        obj.btnLanzar.addEventListener('click', m.animarLanzaDado())

        setTimeout(() => {
            m.mostrarPregunta()
            obj.caraSeleccionada.style.opacity = 0
        }, 4000);
    },
   

    animarLanzaDado: function(){
        //let cara;
        obj.btnLanzar.style.animationDuration = '.8s';
        obj.btnLanzar.classList.add('animated');

        setTimeout(() => {
            obj.btnLanzar.classList.remove('animated');
            obj.categoriaAleatoria = obj.categoriasDisponibles.sort(function() {return Math.random() - 0.5})
            switch (obj.categoriaAleatoria[0]) {                
                case 1:
                    m.mostrarCara('114px', '30px');
                    break;

                case 2:
                    m.mostrarCara('114px', '130px');
                    break;

                case 3:
                    m.mostrarCara('114px', '229px');        
                    break;

                case 4:
                    m.mostrarCara('218px', '30px');
                    break;

                case 5:
                    m.mostrarCara('218px', '130px');
                    break;

                case 6:
                    m.mostrarCara('218px', '229px')
                    break;   

                default:
                    break;
            }
        }, 2000);    
    },
    

    mostrarCara: function(top, left){
        obj.caraSeleccionada.style.opacity = 1
        obj.caraSeleccionada.style.top = top
        obj.caraSeleccionada.style.left = left
    },


    mostrarPregunta: () => {      

        // Necesito detener toda la animacion....
        obj.intro.style.display = 'none';
        obj.instrucciones.style.display = 'none';
        obj.registro.style.display = 'none';
        obj.lanzarDado.style.display = 'none';
        obj.mostrarRespuestas.style.display = 'none';
        obj.botonera1.style.display = 'none';
        obj.mostrarPregunta.style.display = 'block';
        obj.botonera.style.display = 'flex';

        clearInterval(obj.intervaloTiempo);
        obj.seg15 = 15

        m.preguntaPorCategoria(obj.categoriaAleatoria[0]);
        m.funcion15();
    },



    preguntaPorCategoria: async function(cat = 0){
        switch (cat) {
            case 1:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat1.length, 0);
                obj.preguntaActual = obj.preguntasCat1[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat1, obj.preguntaAleatoria);
                break;

            case 2:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat2.length, 0);
                obj.preguntaActual = obj.preguntasCat2[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat2, obj.preguntaAleatoria);
                break;

            case 3:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat3.length, 0);
                obj.preguntaActual = obj.preguntasCat3[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat3, obj.preguntaAleatoria);
                break;
       
            case 4:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat4.length, 0);
                obj.preguntaActual = obj.preguntasCat4[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat4, obj.preguntaAleatoria);
                break;
            
            case 5:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat5.length, 0);
                obj.preguntaActual = obj.preguntasCat5[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat5, obj.preguntaAleatoria);
                break;

            case 6:
                obj.preguntaAleatoria = m.aleatorio(obj.preguntasCat6.length, 0);
                obj.preguntaActual = obj.preguntasCat6[obj.preguntaAleatoria];
                m.mostrarPreguntasyRespuestas(obj.preguntasCat6, obj.preguntaAleatoria);
               break;

           default:
               break;
        }
    },


    mostrarPreguntasyRespuestas: async function(arregloPreguntas, pregunta){
        
        const arregloTemp = [1,2,0];

        obj.preguntaHTML.innerHTML = arregloPreguntas[pregunta].question;

        const arregloRespuestas = arregloPreguntas[pregunta].answers;

        const arregloTemp1 = arregloTemp.sort(function() { return Math.random() - 0.5 });

        // Mostrar respuestas
        obj.opcionA.innerHTML = arregloRespuestas[arregloTemp1[0]];
        obj.opcionB.innerHTML = arregloRespuestas[arregloTemp1[1]];
        obj.opcionC.innerHTML = arregloRespuestas[arregloTemp1[2]];        
    },


    eliminarPregunta: function( categoria, posicion){
        switch (categoria) {
            case 1:
                obj.preguntasCat1.splice(posicion,1);
                break;
            case 2:
                obj.preguntasCat2.splice(posicion,1);
                break;
            case 3:
                obj.preguntasCat3.splice(posicion,1);
                break;
            case 4:
                obj.preguntasCat4.splice(posicion,1);
                break;
            case 5:
                obj.preguntasCat5.splice(posicion,1);
                break;
            case 6:
                obj.preguntasCat6.splice(posicion,1);
                break;
        
            default:
                break;
        }
    },


    aleatorio: function(max, min) {
        return Math.floor(Math.random() * (max - min)) + min;
    },
 
    
    funcion15: function() {

        obj.contadorSeg.innerHTML = obj.seg15
        obj.intervaloTiempo = setInterval(() => {
            
            obj.contadorSeg.innerHTML = obj.seg15;
    
                if(obj.seg15 < 10){
                    obj.contadorSeg.innerHTML = `0${obj.seg15}`  
                }
    
                if(obj.seg15 === 0){
                    clearInterval(obj.intervaloTiempo );
                    console.log('Debe limpiar');
                    
                    m.mostrarRespuestas();
                    //obj.puntos = 1;
                }

                obj.seg15--;

        }, 1000);

    },


    funcion20: function() {
        obj.contadorSeg.innerHTML = obj.seg20

        obj.intervaloTiempo = setInterval(() => {
            
            obj.contadorSeg.innerHTML = obj.seg20
            
                if(obj.seg20 < 10){
                    obj.contadorSeg.innerHTML = `0${obj.seg20}`  
                }
    
                if(obj.seg20 === 0){
                    clearInterval(obj.intervaloTiempo );
                    obj.puntos = 0;
                    m.mostrarPosiciones();    
                }
                obj.seg20--;
        }, 1000);
    },


    sinOpciones: function(){
        m.mostrarCorrectoIncorrecto();

        clearInterval(obj.intervaloTiempo);
        obj.seg20 = 20;
        m.funcion20();
    },


    conOpciones: function(){
        m.mostrarRespuestas();

        clearInterval(obj.intervaloTiempo);
        obj.seg20 = 20;
        m.funcion20();
    },


    constestarConOpciones: function(e){

        if (e.target.innerHTML === obj.preguntaActual.answers[0]){

            obj.puntos = 1;
            
        }
        else{
            obj.puntos = 0;
        }

        m.mostrarPosiciones();
    },

    
    correcto: function(){
        clearInterval(obj.intervaloTiempo);
        obj.puntos = 2;
        m.mostrarPosiciones();
    },


    incorrecto: function(){
        clearInterval(obj.intervaloTiempo);
        obj.puntos = 0;
        m.mostrarPosiciones();
    },


    mostrarRespuestas: function(){

        obj.botonera.style.display = 'none';
        obj.mostrarRespuestas.style.display = 'block';

        //Clic para validar opcion seleccionado...
        obj.opcionA.addEventListener('click', m.constestarConOpciones);
        obj.opcionB.addEventListener('click', m.constestarConOpciones);
        obj.opcionC.addEventListener('click', m.constestarConOpciones);

        clearInterval(obj.intervaloTiempo);
        obj.seg20 = 20;
        m.funcion20();
    },


    mostrarCorrectoIncorrecto: function(){

        //obj.botonera.style.display = 'none';
        //console.log(obj.mostrarRespuestas);
        obj.botonera.style.display = 'none';
        obj.botonera1.style.display = 'flex';

        //m.contadorSegundos(20);
        //setInterval('m.contadorSegundos(20)',1000);
        clearInterval(obj.intervaloTiempo);
        obj.seg20 = 20
        m.funcion20()

    },


    mostrarPosiciones: function(){

        obj.intro.style.display = 'none';
        obj.instrucciones.style.display = 'none';
        obj.registro.style.display = 'none';
        obj.lanzarDado.style.display = 'none';
        obj.mostrarPregunta.style.display = 'none';

        obj.tablaPosiciones.style.display = 'block'

        // Reiniciar variables...
        clearInterval(obj.intervaloTiempo);
        
        m.eliminarPregunta(obj.categoriaAleatoria[0], obj.preguntaAleatoria);
        
        if (obj.puntos === 0){

            obj.puntosError = obj.puntosError + 1;
            m.posicionNegro(obj.puntosError)

        } else{

            m.sumarPuntaje(obj.equipoActual);

        }

        m.verificarExistencias(obj.categoriaAleatoria[0]);

        if (obj.puntosEquipo1 >= 10 || obj.puntosEquipo2 >= 10 || obj.puntosEquipo3 >= 10 || obj.puntosEquipo4 >= 10){

            obj.btnSiguienteTurno.style.display = 'none';

            setTimeout(() => {

                obj.tablaPosiciones.style.display = 'none'
                obj.tableroPremiacion.style.display = 'block';

                m.ganadores()

                obj.primerImg.src = `../recursos/premiacion/fichas/${obj.premiacion[0].imagen}`;
                obj.primerLabel.innerHTML = obj.premiacion[0].nombre;

                obj.segundoImg.src = `../recursos/premiacion/fichas/${obj.premiacion[1].imagen}`;
                obj.segundoLabel.innerHTML = obj.premiacion[1].nombre;

                obj.tercerImg.src = `../recursos/premiacion/fichas/${obj.premiacion[2].imagen}`;
                obj.tercerLabel.innerHTML = obj.premiacion[2].nombre;

                obj.cuartoImg.src = `../recursos/premiacion/fichas/${obj.premiacion[3].imagen}`;
                obj.cuartoLabel.innerHTML = obj.premiacion[3].nombre;
                
            }, 2000);
        }        
    },


    siguienteTurno: async function(){
        //console.log('Mostrar tabla de posiciones...');
        obj.intro.style.display = 'none';
        obj.instrucciones.style.display = 'none';
        obj.registro.style.display = 'none';
        obj.mostrarPregunta.style.display = 'none';
        
        obj.tablaPosiciones.style.display = 'none'        
        obj.lanzarDado.style.display = 'block';
        obj.recorrerOrden++;
        // Reiniciar equipo...
        obj.puntos = 0;

        console.log('Desde Siguiente turno: ', obj.categoriasDisponibles.length);

        // Si se acaban TODAS las preguntas... cargar de nuevo...
        if(obj.categoriasDisponibles.length === 0){

            obj.datosJSON = await m.cargarDatosJSON('GET', 'js/file.json');

            m.llenarPreguntas();
            obj.categoriasDisponibles = [1,2,3,4,5,6];
        }

        m.mostrarEquipo();        
    },


    verificarExistencias: function(categoria){
        switch (categoria) {
            case 1:
                if(obj.preguntasCat1.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 1)
                }
                break;
            case 2:
                if(obj.preguntasCat2.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 2)
                }
                break;
            case 3:
                if(obj.preguntasCat3.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 3)
                }
                break;
            case 4:
                if(obj.preguntasCat4.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 4)
                }
                break;
            case 5:
                if(obj.preguntasCat5.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 5)
                }
                break;
            case 6:
                if(obj.preguntasCat6.length === 0){
                    m.eliminarCategoria(obj.categoriasDisponibles, 6)
                }
                break;
            default:
                break;
        }
    },


    eliminarCategoria: function(arreglo, categoria){
        let indice = arreglo.indexOf(categoria);
        arreglo.splice(indice, 1);
    },


    sumarPuntaje: function(equipo){
        switch (equipo) {
            case 1:
                obj.puntosEquipo1 = obj.puntosEquipo1 + obj.puntos;
                m.posicionVerde(obj.puntosEquipo1);
                break;
            
            case 2:
                obj.puntosEquipo2 = obj.puntosEquipo2 + obj.puntos;
                m.posicionAzul(obj.puntosEquipo2);
                break;

            case 3:
                obj.puntosEquipo3 = obj.puntosEquipo3 + obj.puntos;
                m.posicionAmarillo(obj.puntosEquipo3);
                break;  

            case 4:
                obj.puntosEquipo4 = obj.puntosEquipo4 + obj.puntos;
                m.posicionRojo(obj.puntosEquipo4);
                break;
        
            default:
                break;
        }
    },


    moverFicha: function (obj, left, top){
        obj.style.left = left;
        obj.style.top = top;

        obj.style.animationDuration = '.1s';
        obj.classList.add('animar-parpadeo');

        setTimeout(() => {
            obj.classList.remove('animar-parpadeo');
        }, 1000);
    },


    posicionVerde:function(posicion){
        switch (posicion) {
            case 1:
                m.moverFicha(obj.fichaVerde, '242px', '277px');
                break;
        
            case 2:
                m.moverFicha(obj.fichaVerde, '436px', '252px');
                break;
    
            case 3:
                m.moverFicha(obj.fichaVerde, '612px', '231px');
                break;
            
            case 4:
                m.moverFicha(obj.fichaVerde, '657px', '347px');
                break;
    
            case 5:
                m.moverFicha(obj.fichaVerde, '475px', '370px');
                break;
    
            case 6:
                m.moverFicha(obj.fichaVerde, '268px', '360px');
                break;
            
            case 7:
                m.moverFicha(obj.fichaVerde, '98px', '450px');
                break;
    
            case 8:
                m.moverFicha(obj.fichaVerde, '275px', '528px');
                break;
    
            case 9:
                m.moverFicha(obj.fichaVerde, '475px', '504px');
                break;
    
            case 10:
                m.moverFicha(obj.fichaVerde, '675px', '479px');
                break;
    
            default:
                m.moverFicha(obj.fichaVerde, '758px', '440px');
                break;
        }
    },


    posicionAzul:function(posicion){
        switch (posicion) {
            case 1:
                m.moverFicha(obj.fichaAzul, '228px', '242px');
                break;
        
            case 2:
                m.moverFicha(obj.fichaAzul, '415px', '217px');
                break;
	    
            case 3:
                m.moverFicha(obj.fichaAzul, '648px', '202px');
                break;
            
            case 4:
                m.moverFicha(obj.fichaAzul, '636px', '300px');
                break;
    
            case 5:
                m.moverFicha(obj.fichaAzul, '462px', '335px');
                break;
    
            case 6:
                m.moverFicha(obj.fichaAzul, '282px', '402px');
                break;
            
            case 7:
                m.moverFicha(obj.fichaAzul, '166px', '442px');
                break;
    
            case 8:
                m.moverFicha(obj.fichaAzul, '262px', '486px');
                break;
    
            case 9:
                m.moverFicha(obj.fichaAzul, '462px', '463px');
                break;
    
            case 10:
                m.moverFicha(obj.fichaAzul, '662px', '438px');
                break;
    
            default:
                m.moverFicha(obj.fichaAzul, '758px', '440px');
                break;        	
        }
    },

    
    posicionAmarillo:function(posicion){
        switch (posicion) {
            case 1:
                m.moverFicha(obj.fichaAmarillo, '150px', '253px');
                break;
        
            case 2:
                m.moverFicha(obj.fichaAmarillo, '341px', '228px');
                break;
    
            case 3:
                m.moverFicha(obj.fichaAmarillo, '541px', '194px');
                break;
            
            case 4:
                m.moverFicha(obj.fichaAmarillo, '697px', '271px');
                break;
    
            case 5:
                m.moverFicha(obj.fichaAmarillo, '538px', '325px');
                break;
    
            case 6:
                m.moverFicha(obj.fichaAmarillo, '338px', '348px');
                break;
                	
            case 7:
                m.moverFicha(obj.fichaAmarillo, '98px', '404px');
                break;
    
            case 8:
                m.moverFicha(obj.fichaAmarillo, '204px', '539px');
                break;
    
            case 9:
                m.moverFicha(obj.fichaAmarillo, '404px', '518px');
                break;
    
            case 10:
                m.moverFicha(obj.fichaAmarillo, '604px', '494px');
                break;
    
            default:
                m.moverFicha(obj.fichaAmarillo, '758px', '440px');
                break;
        }
    },

    posicionRojo:function(posicion){
        switch (posicion) {
            case 1:
                m.moverFicha(obj.fichaRojo, '169px', '289px');
                break;
        
            case 2:
                m.moverFicha(obj.fichaRojo, '362px', '265px');
                break;

            case 3:
                m.moverFicha(obj.fichaRojo, '559px', '231px');
                break;
            
            case 4:
                m.moverFicha(obj.fichaRojo, '717px', '317px');
                break;
    
            case 5:
                m.moverFicha(obj.fichaRojo, '544px', '363px');
                break;
    
            case 6:
                m.moverFicha(obj.fichaRojo, '351px', '386px');
                break;
            
            case 7:
                m.moverFicha(obj.fichaRojo, '160px', '400px');
                break;

            case 8:
                m.moverFicha(obj.fichaRojo, '199px', '497px');
                break;
    
            case 9:
                m.moverFicha(obj.fichaRojo, '339px', '476px');
                break;
    
            case 10:
                m.moverFicha(obj.fichaRojo, '600px', '451px');
                break;
    
            default:
                m.moverFicha(obj.fichaRojo, '758px', '440px');
                break;
        }
    },


    posicionNegro:function(posicion){
        switch (posicion) {
            case 1: 
                m.moverFicha(obj.fichaNegro, '199px', '267px');
                break;
        
            case 2:
                m.moverFicha(obj.fichaNegro, '391px', '242px');
                break;
    
            case 3:
                m.moverFicha(obj.fichaNegro, '592px', '198px');
                break;
            
            case 4:
                m.moverFicha(obj.fichaNegro, '676px', '309px');
                break;

            case 5:
                m.moverFicha(obj.fichaNegro, '503px', '345px');
                break;
    
            case 6:
                m.moverFicha(obj.fichaNegro, '308px', '375px');
                break;
            
            case 7:
                m.moverFicha(obj.fichaNegro, '130px', '425px');
                break;
    
            case 8:
                m.moverFicha(obj.fichaNegro, '235px', '516px');
                break;
    
            case 9:
                m.moverFicha(obj.fichaNegro, '435px', '493px');
                break;
    
            case 10:
                m.moverFicha(obj.fichaNegro, '635px', '468px');
                break;

            default:
                m.moverFicha(obj.fichaNegro, '758px', '440px');
                break;
        }
    },

    
    ganadores: function(){
        const posiciones = [{
            equipo:1,
            imagen: 'green.png',
            nombre: obj.nombresEquipo[0],
            puntos: obj.puntosEquipo1 
        },
        {
            equipo:2,
            imagen: 'blue.png',
            nombre: obj.nombresEquipo[1],
            puntos: obj.puntosEquipo2
        },
        {
            equipo:3,
            imagen: 'yellow.png',
            nombre: obj.nombresEquipo[2],
            puntos: obj.puntosEquipo3
        },
        {
            equipo:4,
            imagen: 'red.png',
            nombre: obj.nombresEquipo[3],
            puntos: obj.puntosEquipo4
        }
    ];
    obj.premiacion = posiciones.sort(((a, b) => b.puntos - a.puntos));
    }
}

m.introMascotas();