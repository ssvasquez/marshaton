# Marshaton

Juego Corporativo

# Juego Mars

```
Descargar y ejecutar el juego en un servidor local para que funcione el JSON.
```

# Logica del juego.

1. Para iniciar el juego deberán reunir 4 jugadores o 4 equipos, cada equipo elegirá su nombre y el color de su ficha en el tablero (verde, azul, amarillo y rojo).

2. El turno de los equipos se decidirá de forma aleatoria.

3. Cuando sea el turno, el equipo deberá tirar el dado, el número que le toque determinará la categoría de la pregunta. 

4. Cuando la pregunta aparezca, el equipo tendrá solo 15 segundos para elegir si quiere responderla con opciones o sin opciones.

5. Si elige constestar sin opciones y contesta correctamente, avanzará 2 casillas, si elige contestar con opciones, y contesta correctamente, avanzará 1 casilla. En ambos casos contará con 20 segundos para dar su respuesta al juez. Si la respuesta que da el equipo es incorrecta, la ficha de color negra avazará 1 casilla.

6. Para las preguntas que se respondan sin opciones, el juez del juego tendrá la responsabilidad de decidir si la respuesta proporcioanada es correcta o incorrecta.

7. Ganará el juego el equipo que avance las 10 casillas del tablero y llegue a la meta. Si la ficha negra llega en primer lugar, el juego no terminará hasta que un equipo llegue a la meta.
